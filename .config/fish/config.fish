# env vars
set -gx XDG_CONFIG_HOME $HOME/.config
set -gx XDG_CACHE_HOME $HOME/.cache
set -gx XDG_DATA_HOME $HOME/.local/share
set -gx XDG_STATE_HOME $HOME/.local/state

# ssh agent is started by sway, not dependent on :DISPLAY
# ideally this variable would only be set after ssh agent starts but there doesn't seem to be any way to do that
set -gx SSH_AUTH_SOCK $XDG_RUNTIME_DIR/ssh-agent-$XDG_SESSION_ID.sock
set -gx SSH_ASKPASS lxqt-openssh-askpass

set -gx EDITOR kak
set -gx PAGER less
set -gx MANPAGER $PAGER
set -gx TERMINAL foot
set -gx BROWSER librewolf
set -gx QT_QPA_PLATFORMTHEME qt5ct
set -gx _JAVA_OPTIONS '-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java -Dawt.useSystemAAFontSettings=gasp'
set -gx LESS "-FR --mouse --wheel-lines=15 --use-color"
set -gx LS_COLORS "di=01;94"
set -gx GTK_THEME "oomox-darker-than-fusion" # required for librewolf/firefox to theme properly
set -gx SAL_USE_VCLPLUGIN qt6 # use qt for libreoffice

set -gx DEVKITPRO /opt/devkitpro
set -gx DEVKITARM $DEVKITPRO/devkitARM
set -gx DEVKITPPC $DEVKITPRO/devkitPPC
set -a PATH $DEVKITARM/bin/ $DEVKITPPC/bin/ $DEVKITPRO/tools/bin/
set -gx WONDERFUL_TOOLCHAIN /opt/wonderful
set -a PATH $WONDERFUL_TOOLCHAIN/bin

# cargo-nds requires this
set -a PATH $WONDERFUL_TOOLCHAIN/toolchain/gcc-arm-none-eabi/bin
set -a PATH $WONDERFUL_TOOLCHAIN/thirdparty/blocksds/core/tools/ndstool

set -gx BLOCKSDS $WONDERFUL_TOOLCHAIN/thirdparty/blocksds/core
set -gx BLOCKSDSEXT $WONDERFUL_TOOLCHAIN/thirdparty/blocksds/external

# sccache breaks on some flags
#set -gx CC sccache -- gcc
#set -gx CXX sccache -- g++
set -gx RUSTC_WRAPPER sccache
set -gx CARGO_INCREMENTAL false

# fix directories
set -gx RUSTUP_HOME $XDG_DATA_HOME/rustup
set -gx CARGO_HOME $XDG_DATA_HOME/cargo
#set -gx RBENV_ROOT $XDG_DATA_HOME/rbenv
set -gx PNPM_HOME $XDG_DATA_HOME/pnpm
set -gx NPM_CONFIG_USERCONFIG $XDG_CONFIG_HOME/npm/npmrc
set -gx NODE_REPL_HISTORY $XDG_DATA_HOME/node/history
set -gx GTK2_RC_FILES $XDG_CONFIG_HOME/gtk-2.0/gtkrc
set -gx WINEPREFIX $XDG_DATA_HOME/wineprefixes/default
set -gx XAUTHORITY $XDG_RUNTIME_DIR/Xauthority

set -a PATH $HOME/.local/bin $XDG_DATA_HOME/cargo/bin $HOME/.nix-profile/bin
set -a PATH $PNPM_HOME

if status is-interactive
	tabs -3
	function fish_greeting; end

	set -l swayvars \
		XDG_CURRENT_DESKTOP=sway \
		QT_QPA_PLATFORM=wayland \
		ELM_DISPLAY=wl \
		MOZ_ENABLE_WAYLAND=1 \
		SDL_VIDEODRIVER="wayland,x11"

	function rootconf
		ln -s $HOME/.themes /root/.themes # only way i can find to theme gparted
		cp ~/.config/xkb/symbols/sarpnt /usr/share/X11/xkb/symbols/sarpnt # keyboard layout
		 # vconsole keyboard layout
		#kbdrate -d 200 -r 60 # vconsole keyboard repeat
	end
	function wayland
		env $swayvars $argv
	end
	function webcam
		mpv av://v4l2:/dev/video$argv[1] --profile=low-latency --untimed
	end
	# function to work around autocomplete freezing
	function conf --wraps git
		git --git-dir="$HOME/.dotfiles" --work-tree="$HOME" $argv
	end
	function clean
		p -Rns (p -Qdtq)
		p -Sc
	end

	function ks --description "kak session"
		command kak -clear
		pushd $XDG_RUNTIME_DIR
		set -l session (mktemp -u ks-XXXXXXXXXX)
		popd
		command kak -d -s $session &
		#echo "new lsp-hover-buffer" | kak -p $session
		kc $session
		kill %1
	end
	function kc --description "kak connect"
		set -lx fish_color_cwd ee88ee
		fish -C "alias kak 'kak -c $argv[1]'"
	end

	function _sudo-doas-alias --on-variable PATH
		if ! command -q doas && command -q sudo
			abbr --erase sudo
			abbr --add doas sudo
		else if ! command -q sudo && command -q doas
			abbr --add sudo doas
			abbr --erase doas
		else
			abbr --erase sudo
			abbr --erase doas
		end
	end
	_sudo-doas-alias

	alias ip4 "curl https://ipv4.myip.wtf/text"
	alias ip6 "curl https://ipv6.myip.wtf/text"
	alias ipd "curl https://myip.wtf/text"
	alias p "paru"
	alias up "topgrade"
	alias ip "ip --color=auto"
	alias diff "delta"
	alias l "ls -alXh --color --group-directories-first"
	alias ls "ls -Xh --color --group-directories-first"
	alias rm "rm -i"
	alias mv "mv -i"
	alias grep "rg"
	alias fakedim "gammastep -O 6500K -b"
	alias mpv "mpv --ytdl-format='bestvideo[height<=?1080]+bestaudio'"
	function m
		node -e "console.log($argv)"
	end

	stty erase \^\?

	zoxide init fish | source
	thefuck --alias | source
	#and . (rbenv init - | psub)

	if test (tty) = "/dev/tty1";
		exec env $swayvars sway
	end
end

function prompt_login --description "display user and host for the prompt"
	set -l color_host brgreen
	if set -q SSH_TTY; and set -q fish_color_host_remote
		set color_host yellow
	end

	if test $USER != "sarpnt"
		echo -ns (set_color brmagenta) $USER (set_color normal)
	end
	echo -ns @ (set_color $color_host) (prompt_hostname) (set_color normal)
end

function fish_prompt --description "Write out the prompt"
	set -l last_pipestatus $pipestatus
	set -lx __fish_last_status $status # Export for __fish_print_pipestatus.

	set -q fish_color_cwd
	or set -g fish_color_cwd green

	set -q __fish_prompt_status_generation
	or set -g __fish_prompt_status_generation $status_generation

	# If the pipestatus was carried over (e.g. after `set`), don't bold it.
	set -l bold_flag --bold
	if test $__fish_prompt_status_generation = $status_generation
		set bold_flag
	end

	set __fish_prompt_status_generation $status_generation
	set -l prompt_status (__fish_print_pipestatus "[" "]" "|" (set_color red) (set_color $bold_flag red) $last_pipestatus)

	echo -ns (prompt_login) " " (set_color $fish_color_cwd) (prompt_pwd) (set_color normal) (fish_vcs_prompt) " "$prompt_status " "
end

set -g fish_color_normal normal
set -g fish_color_command eea
set -g fish_color_keyword e8e
set -g fish_color_quote fa7
set -g fish_color_redirection normal
set -g fish_color_end normal
set -g fish_color_error f56
set -g fish_color_param ada
set -g fish_color_option 79f
set -g fish_color_comment 5c4
set -g fish_color_operator 9de # includes variables
set -g fish_color_escape f78
set -g fish_color_match normal # parens

set -g fish_color_valid_path --underline
set -g fish_color_selection --background 247 # erases foreground color, is bold for some reason?
set -g fish_color_autosuggestion brblack
set -g fish_color_cancel --reverse

# TODO what are these
set -g fish_color_search_match bryellow --background brblack
set -g fish_color_history_current --bold

# completion pager
set -g fish_pager_color_progress 5c4 --bold -r

# completion
set -g fish_pager_color_background
set -g fish_pager_color_prefix normal --bold --underline
set -g fish_pager_color_completion normal
set -g fish_pager_color_description 5c4 -i

# selected completion
set -g fish_pager_color_selected_background --background 247
set -g fish_pager_color_selected_prefix
set -g fish_pager_color_selected_completion
set -g fish_pager_color_selected_description

# every second unselected completion
set -g fish_pager_color_secondary_background
set -g fish_pager_color_secondary_prefix
set -g fish_pager_color_secondary_completion
set -g fish_pager_color_secondary_description
