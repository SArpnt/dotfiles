# Read `man 5 sway` for a complete reference.

set $left  n
set $down  e
set $up    i
set $right o

set $mod Mod4

set $menu rofi -modi drun -show drun -sort -sorting-method fzf

font pango:monospace 8

### Output configuration
# You can get the names of your outputs by running: swaymsg -t get_outputs

output * background #000000 solid_color
output * adaptive_sync on
output * allow_tearing yes
# TODO max_render time should be 8 when not tearing but off when tearing? the settings here seem quite bad
output * max_render_time 8
#output * max_render_time off

output "Acer Technologies RX241Y TM9AA0013900 " mode 1920x1080@165Hz pos 0,0 subpixel rgb
output "Chimei Innolux Corporation 0x15F5 Unknown" pos 1920,0 subpixel rgb
# 70hz works but the monitor gets very loud on certain images
#output "Dell Inc. DELL P2210 U828K963B7TS" mode --custom 1680x1050@70Hz pos 3840,0 subpixel rgb
output "Dell Inc. DELL P2210 U828K963B7TS" mode 1680x1050@60Hz pos 3840,0 subpixel rgb
output "Samsung Electric Company Odyssey G40B HCJT700486" mode 1920x1080@120Hz pos 0,0 subpixel rgb

### Idle configuration
#
# Example configuration:
#
# exec swayidle -w \
# 	timeout 300 'swaylock -f -c 000000' \
# 	timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
# 	before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

### Input configuration
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

input "type:keyboard" {
	xkb_layout sarpnt
	xkb_numlock enabled

	repeat_delay 200
	repeat_rate 60

	# most keyboards here are pc104
	xkb_model "pc104"
}
input "1133:49971:Logitech_Gaming_Keyboard_G610_Keyboard" {
}
input "1:1:AT_Translated_Set_2_keyboard" {
	#xkb_model ""
}
# for flat pointer accel:
# -1 is *0 speed (there's actually a .0005 minimum but ignore that)
# 1 is *2 speed
# just subtract 1 from the multiplier to get the correct pointer accel
input "type:pointer" {
	accel_profile "flat"
	pointer_accel 0
}
input "4152:6180:SteelSeries_SteelSeries_Rival_3" {
	pointer_accel -0.875
}
input "type:touchpad" {
	click_method clickfinger
	tap disabled

	dwt disabled

	scroll_method two_finger
	natural_scroll false

	accel_profile adaptive
	pointer_accel 0
}

seat seat0 xcursor_theme Adwaita
#seat laptop xcursor_theme breeze
#seat vnc1 xcursor_theme Wii-Pointer-P1
#seat vnc2 xcursor_theme Wii-Pointer-P2
#seat vnc3 xcursor_theme Wii-Pointer-P3
#seat vnc4 xcursor_theme Wii-Pointer-P4
#seat laptop attach "1:1:AT_Translated_Set_2_keyboard"
#seat laptop attach "1267:12351:ELAN0511:00_04F3:303F_Touchpad"

### Key bindings
# Moving around:
	bindsym $mod+$left  focus left
	bindsym $mod+$down  focus down
	bindsym $mod+$up    focus up
	bindsym $mod+$right focus right
	bindsym $mod+Left  focus left
	bindsym $mod+Down  focus down
	bindsym $mod+Up    focus up
	bindsym $mod+Right focus right

	bindsym $mod+Shift+$left  move left
	bindsym $mod+Shift+$down  move down
	bindsym $mod+Shift+$up    move up
	bindsym $mod+Shift+$right move right
	bindsym $mod+Shift+Left  move left
	bindsym $mod+Shift+Down  move down
	bindsym $mod+Shift+Up    move up
	bindsym $mod+Shift+Right move right

	bindsym $mod+Ctrl+$left  move workspace to output left
	bindsym $mod+Ctrl+$down  move workspace to output down
	bindsym $mod+Ctrl+$up    move workspace to output up
	bindsym $mod+Ctrl+$right move workspace to output right
	bindsym $mod+Ctrl+Left  move workspace to output left
	bindsym $mod+Ctrl+Down  move workspace to output down
	bindsym $mod+Ctrl+Up    move workspace to output up
	bindsym $mod+Ctrl+Right move workspace to output right

# Workspaces:
	bindsym $mod+1 workspace number 1
	bindsym $mod+2 workspace number 2
	bindsym $mod+3 workspace number 3
	bindsym $mod+4 workspace number 4
	bindsym $mod+5 workspace number 5
	bindsym $mod+6 workspace number 6
	bindsym $mod+7 workspace number 7
	bindsym $mod+8 workspace number 8
	bindsym $mod+9 workspace number 9
	bindsym $mod+0 workspace number 0

	bindsym $mod+Shift+1 move container to workspace number 1
	bindsym $mod+Shift+2 move container to workspace number 2
	bindsym $mod+Shift+3 move container to workspace number 3
	bindsym $mod+Shift+4 move container to workspace number 4
	bindsym $mod+Shift+5 move container to workspace number 5
	bindsym $mod+Shift+6 move container to workspace number 6
	bindsym $mod+Shift+7 move container to workspace number 7
	bindsym $mod+Shift+8 move container to workspace number 8
	bindsym $mod+Shift+9 move container to workspace number 9
	bindsym $mod+Shift+0 move container to workspace number 0
	# Note: workspaces can have any name you want, not just numbers.
	# We just use 1-10 as the default.

#
	bindsym $mod+d exec $menu
	bindsym $mod+Return exec $TERMINAL
	bindsym $mod+Shift+q kill
	bindsym $mod+Shift+c reload
	bindsym $mod+Shift+k exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -B 'Yes, exit sway' 'swaymsg exit'

	bindsym $mod+b split n
	bindsym $mod+g split h
	bindsym $mod+v split v

	bindsym $mod+f fullscreen
	bindsym $mod+s layout toggle split
	bindsym $mod+t layout toggle tabbed stacking

	bindsym $mod+p focus parent
	bindsym $mod+c focus child
	bindsym $mod+Shift+space floating toggle
	bindsym $mod+space focus mode_toggle
	bindsym $mod+Shift+minus move scratchpad
	bindsym $mod+minus scratchpad show

# Resizing containers:
bindsym $mod+r mode "resize"
mode "resize" {
	bindsym $left  resize shrink width 10px
	bindsym $down  resize grow height 10px
	bindsym $up    resize shrink height 10px
	bindsym $right resize grow width 10px

	bindsym Left  resize shrink width 10px
	bindsym Down  resize grow height 10px
	bindsym Up    resize shrink height 10px
	bindsym Right resize grow width 10px

	bindsym Return mode "default"
	bindsym Escape mode "default"
}

mode passthrough {
	bindsym $mod+Pause mode default
}
bindsym $mod+Pause mode passthrough
floating_modifier $mod normal

# Status Bar:
# Read `man 5 sway-bar` for more information about this section.
bar {
	position bottom

	# When the status_command prints a new line to stdout, swaybar updates.
	status_command i3blocks
	status_edge_padding 0
	tray_padding 2

	colors {
		statusline #ffffff
		background #000000
		inactive_workspace #32323200 #32323200 #5c5c5c
	}
}

workspace_layout tabbed
default_border normal 1
default_floating_border normal 1
hide_edge_borders both
#no_focus [all]
mouse_warping none
focus_follows_mouse no

# volume
bindsym XF86AudioRaiseVolume exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 2%+ && pkill -RTMIN+10 i3blocks
bindsym XF86AudioLowerVolume exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 2%- && pkill -RTMIN+10 i3blocks
bindsym XF86AudioMute exec wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle && pkill -RTMIN+10 i3blocks
bindsym XF86AudioMicMute exec wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle && pkill -RTMIN+10 i3blocks

# screenshot
# no mods: current screen
# ctrl: select rectangle
# alt: select window
# shift: show cursor
set $slurp slurp -b \#00000000 -w 1 -c \#888888ff -d
set $focused_screen swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name'
set $select_win swaymsg -t get_tree | jq -r '.. | select(.pid? and .visible?) | "\(.rect.x+.window_rect.x),\(.rect.y+.window_rect.y) \(.window_rect.width)x\(.window_rect.height)"'
bindsym Print exec grim -o $($focused_screen) - | wl-copy -t image/png
bindsym Control+Print exec $slurp | grim -g - - | wl-copy -t image/png
bindsym Mod1+Sys_Req exec grim -i | wl-copy -t image/png
bindsym Shift+Print exec grim -c -o $($focused_screen) - | wl-copy -t image/png
bindsym Shift+Control+Print exec $slurp | grim -c -g - - | wl-copy -t image/png
#bindsym Shift+Mod1+Sys_Req exec grim -c -i $(xdotool getactivewindow) | wl-copy -t image/png

bindsym $mod+Print exec grim /tmp/printscrn && feh /tmp/printscrn
bindsym $mod+Control+Print exec $slurp | grim -g - /tmp/printscrn && feh /tmp/printscrn
#bindsym $mod+Mod1+Sys_Req exec grim -i $(xdotool getactivewindow) /tmp/printscrn && feh /tmp/printscrn
bindsym $mod+Shift+Print exec grim -c /tmp/printscrn && feh /tmp/printscrn
bindsym $mod+Shift+Control+Print exec $slurp | grim -c -g - /tmp/printscrn && feh /tmp/printscrn
#bindsym $mod+Shift+Mod1+Sys_Req exec grim -c -i $(xdotool getactivewindow) /tmp/printscrn && feh /tmp/printscrn

bindsym XF86MonBrightnessUp exec xbacklight -perceived -inc 5
bindsym XF86MonBrightnessDown exec xbacklight -perceived -dec 5
bindsym Shift+XF86MonBrightnessUp exec xbacklight -inc 5
bindsym Shift+XF86MonBrightnessDown exec xbacklight -dec 5

xwayland enable

# fix xwayland backspace key repeat
exec xmodmap -e "clear Lock"

exec ssh-agent -D -a $XDG_RUNTIME_DIR/ssh-agent-$XDG_SESSION_ID.sock
#exec xrdb ~/.config/X11/Xresources
exec xsettingsd
exec lxqt-policykit-agent
#exec pipewire
#exec pipewire-pulse
#exec wireplumber

# TODO i think xdg-desktop portal just starts the other ones?
# clean this up at some point
exec /usr/lib/xdg-desktop-portal
exec /usr/lib/xdg-desktop-portal-hyprland
exec /usr/lib/xdg-desktop-portal-lxqt

exec dunst
exec pcmanfm-qt -d

exec qbittorrent --no-splash
exec keepassxc
#exec nheko
#exec $TERMINAL aerc

for_window [instance="^xeyes$"] floating enable
for_window [app_id="ssh-askpass$"] floating enable
for_window [title="^KeePassXC - Passkey credentials$"] floating enable
for_window [app_id="^lxqt-policykit-agent$"] floating enable
for_window [app_id="^lxqt-archiver$" title="^Progress$"] floating enable
for_window [app_id="^qarma$"] floating enable

include @sysconfdir@/sway/config.d/*
