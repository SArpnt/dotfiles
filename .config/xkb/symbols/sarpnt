default
xkb_symbols "basic" {
	name[group1]= "English (SArpnt Colemak)";

	key.type[group1] = "FOUR_LEVEL_ALPHABETIC";

	key <TLDE> {[ grave,   asciitilde,   dead_tilde,       VoidSymbol ]};
	key <AE01> {[ 1,       exclam,       exclamdown,       onesuperior ]};
	key <AE02> {[ 2,       at,           masculine,        twosuperior ]};
	key <AE03> {[ 3,       numbersign,   ordfeminine,      threesuperior ]};
	key <AE04> {[ 4,       dollar,       cent,             sterling ]};
	key <AE05> {[ 5,       percent,      EuroSign,         yen ]};
	key <AE06> {[ 6,       asciicircum,  hstroke,          Hstroke ]};
	key <AE07> {[ 7,       ampersand,    eth,              ETH ]};
	key <AE08> {[ 8,       asterisk,     thorn,            THORN ]};
	key <AE09> {[ 9,       parenleft,    leftsinglequotemark,  leftdoublequotemark ]};
	key <AE10> {[ 0,       parenright,   rightsinglequotemark, rightdoublequotemark ]};
	key <AE11> {[ minus,   underscore,   endash,           emdash ]};
	key <AE12> {[ equal,   plus,         multiply,         division ]};
	// tf2 will only backspace with the backspace key,
	// and it won't work if backspace is set to Hyper
	//key <BKSP> {[ Hyper_R ], type[group1]="ONE_LEVEL"};
	key <BKSP> {[ BackSpace ], type[group1]="ONE_LEVEL"};

	key <TAB>  {[ Tab, ISO_Left_Tab ]};
	key <AD01> {[ q,           Q,         adiaeresis,    Adiaeresis ]};
	key <AD02> {[ w,           W,         aring,         Aring ]};
	key <AD03> {[ f,           F,         atilde,        Atilde ]};
	key <AD04> {[ p,           P,         oslash,        Oslash ]};
	key <AD05> {[ b,           B,         dead_breve,    VoidSymbol ]};
	key <AD06> {[ bracketleft, braceleft, guillemotleft, 0x1002039 ]};
	key <AD07> {[ j,           J,         dstroke,       Dstroke ]};
	key <AD08> {[ l,           L,         lstroke,       Lstroke ]};
	key <AD09> {[ u,           U,         uacute,        Uacute ]};
	key <AD10> {[ y,           Y,         udiaeresis,    Udiaeresis ]};
	key <AD11> {[ apostrophe,  quotedbl,  otilde,        Otilde ]};
	key <AD12> {[ semicolon,   colon,     odiaeresis,    Odiaeresis ]};
	key <BKSL> {[ backslash,   bar,       VoidSymbol,    brokenbar ]};

	key <CAPS> {[ BackSpace ], type[group1]="ONE_LEVEL"};
	key <AC01> {[ a,            A,          aacute,         Aacute ]};
	key <AC02> {[ r,            R,          dead_grave,     VoidSymbol ]};
	key <AC03> {[ s,            S,          ssharp,         0x1001e9e ]};
	key <AC04> {[ t,            T,          dead_acute,     dead_doubleacute ]};
	key <AC05> {[ g,            G,          dead_ogonek,    VoidSymbol ]};
	key <AC06> {[ bracketright, braceright, guillemotright, 0x100203a ]};
	key <AC07> {[ m,            M,          dead_macron,    VoidSymbol ]};
	//key <AC08> {[ n,            N,          Left,           Left,  ntilde, Ntilde, VoidSymbol, VoidSymbol ]};
	//key <AC09> {[ e,            E,          Down,           Down,  eacute, Eacute, VoidSymbol, VoidSymbol ]};
	//key <AC10> {[ i,            I,          Up,             Up,    iacute, Iacute, VoidSymbol, VoidSymbol ]};
	//key <AC11> {[ o,            O,          Right,          Right, oacute, Oacute, VoidSymbol, VoidSymbol ]};
	key <AC08> {[ n,            N,          Left,           Left ]};
	key <AC09> {[ e,            E,          Down,           Down ]};
	key <AC10> {[ i,            I,          Up,             Up ]};
	key <AC11> {[ o,            O,          Right,          Right ]};
	key <RTRN> {[ Return ], type[group1]="ONE_LEVEL"};

	key <LFSH> {[ Shift_L ], type[group1]="ONE_LEVEL"};
	key <LSGT> {[ z,      Z,        ae,              AE ]}; // does not always exist
	key <AB01> {[ x,      X,        dead_circumflex, VoidSymbol ]};
	key <AB02> {[ c,      C,        ccedilla,        Ccedilla ]};
	key <AB03> {[ d,      D,        dead_diaeresis,  VoidSymbol ]};
	key <AB04> {[ v,      V,        oe,              OE ]};
	key <AB05> {[ z,      Z,        ae,              AE ]};
	key <AB06> {[ slash,  question, questiondown,    VoidSymbol ]};
	key <AB07> {[ k,      K,        dead_abovering,  VoidSymbol ]};
	key <AB08> {[ h,      H,        dead_caron,      VoidSymbol ]};
	key <AB09> {[ comma,  less,     dead_cedilla,    VoidSymbol ]};
	key <AB10> {[ period, greater,  dead_abovedot,   VoidSymbol ]};
	key <AB11> {[ slash,  question, questiondown,    VoidSymbol ]}; // does not always exist
	//key <RTSH> {[ ISO_Level5_Shift ], type[group1]="ONE_LEVEL"};
	key <RTSH> {[ Escape ], type[group1]="ONE_LEVEL"};

	key <SPCE> {[ space, space, space, nobreakspace ]};

	key.type[group1] = "ONE_LEVEL";

	key <ESC>  {[ Escape ]};

	key <LCTL> {[ Control_L ]};
	key <LWIN> {[ Super_L ]};
	// TODO what actually is Meta?
	key <LALT> {[ Alt_L, Meta_L ]};
	key <RALT> {[ ISO_Level3_Shift ]};
	key <RWIN> {[ VoidSymbol ]}; // does not always exist
	key <MENU> {[ Menu ]};
	key <RCTL> {[ Hyper_R ]}; // does not always exist
};
