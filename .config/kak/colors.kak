declare-option str fg rgb:eeeeee
declare-option str bg rgb:000000
declare-option str altfg rgb:bbbbbb
declare-option str altbg rgb:111111
declare-option str hidden rgb:444444
declare-option str selection rgb:224477
declare-option str altSelection rgb:226666

declare-option str info blue # TODO
declare-option str hint green # TODO
declare-option str warning yellow # TODO
declare-option str error rgb:ff5566

declare-option str attribute rgb:4ec9b0 # like html attribute
declare-option str macro rgb:ff7788
declare-option str string rgb:ffaa77
declare-option str function rgb:eeeeaa
declare-option str comment rgb:55cc44
declare-option str literal rgb:aaddaa
declare-option str interp rgb:99ddee
declare-option str type rgb:7799ff
declare-option str keyword rgb:ee88ee
#declare-option str yelloworange rgb:d7ba7d
#declare-option str violet rgb:666699

# builtin

set-face global Default "%opt{fg},%opt{bg}"

set-face global PrimaryCursor "+r"
set-face global SecondaryCursor "%opt{altfg}+r"
set-face global PrimaryCursorEol "@PrimaryCursor"
set-face global SecondaryCursorEol "@SecondaryCursor"
set-face global PrimarySelection ",%opt{selection}"
set-face global SecondarySelection ",%opt{altSelection}"

set-face global StatusLine ""
set-face global StatusLineMode ""
set-face global StatusLineInfo "%opt{type}"
set-face global StatusLineValue ""
set-face global StatusCursor "@PrimaryCursor"
set-face global Prompt %opt{type}

set-face global MenuForeground ",%opt{selection}"
set-face global MenuBackground ",%opt{altbg}"
set-face global MenuInfo ""
set-face global Information ",%opt{altbg}"
#InlineInformation

set-face global Error "%opt{error}"
set-face global DiagnosticWarning "%opt{warning}+c"
set-face global DiagnosticError "%opt{error}+c"

set-face global LineNumbers "%opt{altfg}"
set-face global LineNumberCursor ""
set-face global LineNumbersWrapped "%opt{bg}"

set-face global BufferPadding "%opt{bg}" # tildes after the end of the file
set-face global MatchingChar ",%opt{altfg}" # show-matching highlighter TODO
set-face global Whitespace "%opt{hidden}" # show-whitespaces highlighter
set-face global WrapMarker "%opt{hidden}" # wrap-marker highlighter TODO

# syntax highlighting

set-face global attribute "%opt{attribute}"
set-face global builtin "%opt{type}" # TODO
set-face global comment "%opt{comment}"
set-face global documentation "@comment"
set-face global function "%opt{function}"
set-face global keyword "%opt{keyword}"
set-face global meta "%opt{macro}"
set-face global module "%opt{string}" # TODO
set-face global operator ""
set-face global string "%opt{string}"
set-face global type "%opt{type}"
set-face global value "%opt{literal}"
set-face global variable "%opt{interp}"

set-face global block "%opt{string}"
set-face global bullet "%opt{type}"
set-face global header "%opt{type}"
set-face global link "%opt{type}"
set-face global list "%opt{fg}"
set-face global mono "%opt{string}"
set-face global title "%opt{type}"

# lsp

set-face global DiagnosticInfo "%opt{info}+c"
set-face global DiagnosticHint "%opt{hint}+c"
set-face global DiagnosticWarning "%opt{warning}+c"
set-face global DiagnosticError "%opt{error}+c"
set-face global DiagnosticTagDeprecated "+s" # TODO
set-face global DiagnosticTagUnnecessary "+d" # TODO

set-face global InlayDiagnosticInfo "%opt{info}+c"
set-face global InlayDiagnosticHint "%opt{hint}+c"
set-face global InlayDiagnosticWarning "%opt{warning}+c"
set-face global InlayDiagnosticError "%opt{error}+c"

set-face global LineFlagInfo "%opt{info}"
set-face global LineFlagHint "%opt{hint}"
set-face global LineFlagWarning "%opt{warning}"
set-face global LineFlagError "%opt{error}"

set-face global Reference "@MatchingChar"
set-face global ReferenceBind "+u@Reference"

set-face global InlayHint "cyan+d" # TODO
set-face global InlayCodeLens "cyan+d" # TODO

set-face global SnippetsNextPlaceholders "black,green+F" # TODO
set-face global SnippetsOtherPlaceholders "black,yellow+F" # TODO

# tree sitter

set-face global ts_info "%opt{info}+c"
set-face global ts_hint "%opt{hint}+c"
set-face global ts_warning "%opt{warning}+c"
set-face global ts_error "%opt{error}+c"

set-face global ts_attribute "%opt{attribute}"
set-face global ts_comment "%opt{comment}"
#set-face global ts_conceal
set-face global ts_constant "%opt{literal}"
#set-face global ts_constant_builtin_boolean
#set-face global ts_constant_character
set-face global ts_constant_character_escape "%opt{macro}"
#set-face global ts_constant_macro
#set-face global ts_constant_numeric
#set-face global ts_constant_numeric_float
#set-face global ts_constant_numeric_integer
set-face global ts_constructor "%opt{type}"
#set-face global ts_diff_plus
#set-face global ts_diff_minus
#set-face global ts_diff_delta
#set-face global ts_diff_delta_moved
set-face global ts_function "%opt{function}"
#set-face global ts_function_builtin
set-face global ts_function_macro "%opt{macro}"
#set-face global ts_function_method
#set-face global ts_function_special
set-face global ts_keyword "%opt{keyword}"
#set-face global ts_keyword_control
#set-face global ts_keyword_conditional
#set-face global ts_keyword_control_conditional
#set-face global ts_keyword_control_directive
#set-face global ts_keyword_control_import
#set-face global ts_keyword_control_repeat
#set-face global ts_keyword_control_return
#set-face global ts_keyword_control_except
#set-face global ts_keyword_control_exception
#set-face global ts_keyword_directive
#set-face global ts_keyword_function
#set-face global ts_keyword_operator
#set-face global ts_keyword_special
#set-face global ts_keyword_storage
#set-face global ts_keyword_storage_modifier
#set-face global ts_keyword_storage_modifier_mut
#set-face global ts_keyword_storage_modifier_ref
#set-face global ts_keyword_storage_type
#set-face global ts_label
set-face global ts_markup_bold "+b"
set-face global ts_markup_heading "+br"
set-face global ts_markup_italic "+i"
#set-face global ts_markup_list_checked
#set-face global ts_markup_list_numbered
#set-face global ts_markup_list_unchecked
#set-face global ts_markup_list_unnumbered
#set-face global ts_markup_link_label
#set-face global ts_markup_link_url
#set-face global ts_markup_link_uri
#set-face global ts_markup_link_text
#set-face global ts_markup_quote
#set-face global ts_markup_raw
#set-face global ts_markup_raw_block
#set-face global ts_markup_raw_inline
#set-face global ts_markup_strikethrough
set-face global ts_namespace "%opt{string}"
set-face global ts_operator ""
#set-face global ts_property
#set-face global ts_punctuation
#set-face global ts_punctuation_bracket
#set-face global ts_punctuation_delimiter
#set-face global ts_punctuation_special
#set-face global ts_special
#set-face global ts_spell
set-face global ts_string "%opt{string}"
set-face global ts_string_regex "%opt{macro}"
set-face global ts_string_regexp "%opt{macro}"
set-face global ts_string_escape "%opt{macro}"
set-face global ts_string_special "green"
#set-face global ts_string_special_path
#set-face global ts_string_special_symbol
#set-face global ts_string_symbol
#set-face global ts_tag
#set-face global ts_tag_error
#set-face global ts_text
#set-face global ts_text_title
set-face global ts_type "%opt{type}"
#set-face global ts_type_builtin
#set-face global ts_type_enum_variant
set-face global ts_variable "" # TODO
set-face global ts_variable_builtin "%opt{interp}" # TODO
#set-face global ts_variable_other_member
#set-face global ts_variable_parameter
