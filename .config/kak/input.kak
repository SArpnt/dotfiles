# tab completion
map global normal : : -docstring "prompt"
map global prompt <a-tab> <tab> -docstring "next completion"
map global prompt <a-s-tab> <s-tab> -docstring "prev completion"
map global insert <a-tab> <c-n> -docstring "next completion"
map global insert <a-s-tab> <c-p> -docstring "prev completion"

# movement (neioNEIO)
map global normal n h -docstring "left"
map global normal e j -docstring "down"
map global normal i k -docstring "up"
map global normal o l -docstring "right"
map global normal N H -docstring "left"
map global normal E J -docstring "down"
map global normal I K -docstring "up"
map global normal O L -docstring "right"

# move by word and paragraph (luy'L")
map global normal l b -docstring "word left"
#map global normal u -docstring "paragraph down"
#map global normal y -docstring "paragraph up"
map global normal \' e -docstring "word right"
map global normal L <a-b> -docstring "WORD left"
map global normal \" <a-e> -docstring "WORD right"

# switch buffer (UY<>)
map global normal U :delete-buffer<ret>
map global normal Y ":buffer "
map global normal <lt> :buffer-next<ret>
map global normal <gt> :buffer-previous<ret>

# indent
map global normal <a-lt> <lt>
map global normal <a-gt> <gt>

# search (hH)
map global normal h n -docstring "next search"
map global normal H N -docstring "next search"
map global normal <a-h> <a-n> -docstring "prev search"
map global normal <a-H> <a-N> -docstring "prev search"

map global normal , , -docstring "only main selection"
map global normal <a-,> <a-,> -docstring "clear main selection"

# RIGHT SIDE OF KEYBOARD
# UNUSED NORMAL .;
# USED ALT <>hH,
# USED CTRL (none)

# left (arstARST <c-s><c-T>)
map global normal a x
map global normal A <a-x>
map global normal r r
#map global normal R # replace mode # TODO
map global normal s i
map global normal S I
map global normal t a
map global normal T A
map global normal <c-s> O -docstring "insert new line above"
map global normal <c-T> o -docstring "insert new line below"
# <a-o> add an empty line below cursor
# <a-O> add an empty line above cursor

## (zxcdvZXV)
map global normal z u -docstring "undo"
map global normal Z U -docstring "redo"
map global normal x s -docstring "regex select"
map global normal X S -docstring "regex split"
map global normal c y -docstring "copy"
#map global normal C
map global normal v P -docstring "paste"
map global normal V p -docstring "paste after"
map global normal <c-v> "!wl-paste -n<ret>" -docstring "paste clipboard"
map global normal <c-V> "<a-!>wl-paste -n<ret>" -docstring "paste clipboard after"
map global normal <a-v> <a-P> -docstring "paste all"
map global normal <a-V> <a-p> -docstring "paste all after"
map global normal d <a-d> -docstring "delete"
#map global normal D

# (qwfQWF)
map global normal q q -docstring "play macro"
map global normal Q Q -docstring "record macro"
map global normal w <a-c> -docstring "change selection"
map global normal W "|wl-paste -n<ret>" -docstring "paste replace"
map global normal f <a-j> # join lines
map global normal F <a-J>

# marks (all m)
map global normal m z -docstring "save selection"
map global normal M Z -docstring "restore selection"
map global normal M <a-z> -docstring "add selection"
map global normal <c-M> <a-Z> -docstring "reverse add selection"

# select all (%)
map global normal <percent> <percent> -docstring "select all"

# SPECIAL KEYS
# UNUSED NORMAL <backspace> <tab> (todo)
# USED ALT (todo)
# USED CTRL (todo)

# view (jump)
map global normal J V -docstring "lock view"
map global normal j v -docstring "view menu"

map global view j v -docstring "center cursor (vertically)"
map global view n h -docstring "scroll left"
map global view e j -docstring "scroll down"
map global view i k -docstring "scroll up"
map global view o l -docstring "scroll right"
map global view h ""
map global view k ""
map global view l ""
map global view v ""

# goto
map global normal g g -docstring "goto menu"
map global normal G G -docstring "goto menu" # num G needs to extend

map global goto g <esc>ge -docstring "buffer end"
map global goto n <esc>gh -docstring "line begin"
map global goto e <esc>gj -docstring "buffer bottom"
map global goto i <esc>gk -docstring "buffer top"
map global goto o <esc>gl -docstring "line end"
#map global goto ??? <esc>gi -docstring "line non blank start"
map global goto t <esc>gt -docstring "window top"
map global goto c <esc>gc -docstring "window center"
map global goto b <esc>gb -docstring "window bottom"
map global goto G <esc>Ge -docstring "buffer end"
map global goto N <esc>GH -docstring "line begin"
map global goto E <esc>GJ -docstring "buffer bottom"
map global goto I <esc>GK -docstring "buffer top"
map global goto O <esc>GL -docstring "line end"
#map global goto ??? <esc>GI -docstring "line non blank start"
map global goto T <esc>GT -docstring "window top"
map global goto C <esc>GC -docstring "window center"
map global goto B <esc>GB -docstring "window bottom"
map global goto x <esc>:lsp-find-error<ret> -docstring "error"
map global goto w "<esc>:lsp-find-error --include-warnings<ret>" -docstring "warning"
map global goto X "<esc>:lsp-find-error --previous<ret>" -docstring "error"
map global goto W "<esc>:lsp-find-error --previous --include-warnings<ret>" -docstring "warning"


# Insert mode
# <c-r> insert contents of the register given by next key
# <c-v> insert next keystroke directly into the buffer, without interpreting it
# <c-u> commit changes up to now as a single undo group
# <a-;>, <a-semicolon> escape to normal mode for a single command

# Insert mode completion
# <c-o> toggle automatic completion
# <c-n> select next completion candidate
# <c-p> select previous completion candidate
# <c-x> explicit insert completion query, followed by:
#     f explicit file completion
#     w explicit word completion (current buffer)
#     W explicit word completion (all buffers)
#     l explicit line completion (current buffer)
#     L explicit line completion (all buffers)

# \ disable hook for next command

# f select to the next occurrence of given character
# t select until the next occurrence of given character
# <a-[ft]> same as [ft] but in the other direction
# <a-.> repeat last object or f/t selection command
# m select to the next sequence enclosed by matching characters, see the matching_pairs option in :doc options
# M extend the current selection to the next sequence enclosed by matching character, see the matching_pairs option in :doc options
# <a-m> select to the previous sequence enclosed by matching characters, see the matching_pairs option in :doc options
# <a-M> extend the current selection to the previous sequence enclosed by matching characters, see the matching_pairs option in :doc options
# x expand selections to contain full lines (including end-of-lines)
# <a-x> trim selections to only contain full lines (not including last end-of-line)
# <a-h> select to line begin <home> maps to this by default. (See :doc mapping default-mappings)
# <a-l> select to line end <end> maps to this by default. (See :doc mapping default-mappings)
# <pageup>, <c-b>
#     scroll one page up
# <pagedown>, <c-f>
#     scroll one page down
# <c-u> scroll half a page up
# <c-d> scroll half a page down
# ; reduce selections to their cursor
# <a-;> flip the direction of each selection
# <a-:> ensure selections are in forward direction (cursor after anchor)

# Changes
# Yanking (copying) and pasting use the " register by default (See :doc registers)

# . repeat last insert mode change (i, a, or c, including the inserted text)
# I enter insert mode at the beginning of the lines containing the start of each selection
# A enter insert mode at the end of the lines containing the end of each selection
# y yank selections
# p paste after the end of each selection
# P paste before the beginning of each selection
# <a-p> paste all after the end of each selection, and select each pasted string
# <a-P> paste all before the start of each selection, and select each pasted string
# R replace selections with yanked text
# <a-R> replace selections with every yanked text
# <a-j> join selected lines
# <a-J> join selected lines and select spaces inserted in place of line breaks
# <a-_> merge contiguous selections together (works across lines as well)
# <plus> duplicate each selection (generating overlapping selections)
# <a-plus> merge overlapping selections
# <gt> indent selected lines
# <a-gt> indent selected lines, including empty lines
# <lt> unindent selected lines
# <a-lt> unindent selected lines, do not remove incomplete indent (3 leading spaces when indent is 4)
# <c-j> move forward in changes history
# <c-k> move backward in changes history
# <a-u> undo last selection change
# <a-U> redo last selection change
# & align selections, align the cursor of each selection by inserting spaces before the first character of each selection
# <a-&> copy indent, copy the indentation of the main selection (or the count one if a count is given) to all other ones
# _ unselect whitespace surrounding each selection, drop those that only contain whitespace
# <a-)> rotate selections content, if specified, the count groups selections, so the following command
#     3<a-)>
#     rotates (1, 2, 3) and (3, 4, 6) independently
# <a-(> rotate selections content backward
# Changes through external programs

# Shell expansions are available, (See :doc expansions shell-expansions) The default command comes from the | register (See :doc registers)


#! insert and select command output before each selection.
#<a-!> append and select command output after each selection.

# Searching
# Searches use the / register by default (See :doc registers)
# / select next match after each selection
# <a-/> select previous match before each selection
# ? extend to next match after each selection
# <a-?> extend to previous match before each selection
# n select next match after the main selection
# N add a new selection with next match after the main selection
# <a-n> select previous match before the main selection
# <a-N> add a new selection with previous match before the main selection
# * set the search pattern to the main selection (automatically detects word boundaries)
# <a-*> set the search pattern to the main selection (verbatim, no smart detection)

# Jump list
# Some commands, like the goto commands, buffer switch or search commands, push the previous selections to the client's jump list. It is possible to skim through the jump list using:
# <c-i> jump forward
# <c-o> jump backward
# <c-s> create a jump step, but also save the current selection to have it be restored when the step is subsequently cycled through

# Multiple selections
# s, S, <a-k> and <a-K> use the / register by default (See :doc registers)
# s create a selection for each match of the given regex (selects the count capture if it is given)
# S split selections with the given regex (selects the count capture if it is given)
# <a-s> split selections on line boundaries
# <a-S> select first and last characters of each selection
# C duplicate selections on the lines that follow them
# <a-C> duplicate selections on the lines that precede them
# <a-k> keep selections that match the given regex
# <a-K> clear selections that match the given regex
# $ pipe each selection to the given shell command and keep the ones for which the shell returned 0. Shell expansions are available, (See :doc expansions shell-expansions)
# ) rotate main selection (the main selection becomes the next one)
# ( rotate main selection backward (the main selection becomes the previous one)

# Object Selection
# For nestable objects, a count can be used in order to specify which surrounding level to select. Object selections are repeatable using <a-.>.

# Whole object
# A 'whole object' is an object including its surrounding characters. For example, for a quoted string this will select the quotes, and for a word this will select trailing spaces.
# <a-a> select the whole object
# [ select to the whole object start
# ] select to the whole object end
# { extend selections to the whole object start
# } extend selections to the whole object end

# Inner object
# An 'inner object' is an object excluding its surrounding characters. For example, for a quoted string this will not select the quotes, and for a word this will not select trailing spaces.
# <a-i> select the inner object
# <a-[> select to the inner object start
# <a-]> select to the inner object end
# <a-{> extend selections to the inner object start
# <a-}> extend selections to the inner object end

# Objects types

# After the keys described above, a second key needs to be entered in order to specify the wanted object:
# b, (, )
#     select the enclosing parenthesis
# B, {, }
#     select the enclosing {} block
# r, [, ]
#     select the enclosing [] block
# a, <, >
#     select the enclosing <> block
# Q, "
#     select the enclosing double quoted string
# q, '
#     select the enclosing single quoted string
# g, `
#     select the enclosing grave quoted string
# w select the whole word
# <a-w> select the whole WORD
# s select the sentence
# p select the paragraph
# ␣ select the whitespaces
# i select the current indentation block
# n select the number
# u select the argument
# c select user defined object, will prompt for open and close text

# <a-;>, <a-semicolon>
#     run a command with additional expansions describing the selection context (See :doc expansions)

# If a punctuation character is entered, it will act as the delimiter. For instance, if the cursor is on the o of /home/bar, typing <a-a>/ will select /home/.

# Prompt commands
# in the prompt, a transient clipboard is available, its content is empty at the start of prompt edition, and is not preserved afterwards.
# <home>, <c-a>
#     move cursor to first character
# <end>, <c-e>
#     move cursor past the last character
# <a-f> advance to next word begin
# <a-F> advance to next WORD begin
# <a-b> go back to previous word begin
# <a-B> go back to previous WORD begin
# <a-e> advance to next word end
# <a-E> advance to next WORD end
# <c-w> erase to previous word begin, save erased content to clipboard
# <c-W> erase to previous WORD begin, save erased content to clipboard
# <a-d> erase to next word begin, save erased content to clipboard
# <a-D> erase to next WORD begin, save erased content to clipboard
# <c-k> erase to end of line, save erased content to clipboard
# <c-u> erase to begin of line, save erased content to clipboard
# <c-y> insert clipboard content before cursor
# <c-r>
#     insert then content of the register given by next key, if next key has the Alt modifier, it will insert all values in the register joined with spaces, else it will insert the main one
# <c-v>
#     insert next keystroke without interpreting it
# <c-x>
#     explicit completion query, followed by:
#    f  explicit file completion
#    w  explicit word completion (from current buffer)
# <c-o>
#     toggle automatic completion
# <a-!>
#     expand the typed expansions in currently entered text (See :doc expansions)
# <a-;>, <a-semicolon>
#     escape to normal mode for a single command
